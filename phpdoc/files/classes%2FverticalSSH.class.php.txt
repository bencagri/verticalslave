<?php

/**
 * Class using libssh2 extension to open or close SSH connections
 *
 * package     VerticalSlave
 * @category    classes
 * @author      ZalemCitizen
 *
 */

class verticalSSH { 
    // SSH Host 
    public $ssh_host;// = 'myserver.example.com'; 
    // SSH Port 
    public $ssh_port;// = 22; 
    // SSH Server Fingerprint 
    public $ssh_server_fp;// = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'; 
    // SSH Username 
    public $ssh_auth_user;// = 'username'; 
    // SSH Public Key File 
    public $ssh_auth_pub;// = '/home/username/.ssh/id_rsa.pub'; 
    // SSH Private Key File 
    public $ssh_auth_priv;// = '/home/username/.ssh/id_rsa'; 
    // SSH Private Key Passphrase (null == no passphrase) 
    public $ssh_auth_pass; 
    // SSH Connection 
    private $connection; 
    
    public function connect()
    { 
    	$tOutput = array();
        if(!($this->connection = ssh2_connect($this->ssh_host, $this->ssh_port)))
        { 
            throw new Exception(locText('OPENSSH_NOK')); 
        } 
        $fingerprint = ssh2_fingerprint($this->connection, SSH2_FINGERPRINT_MD5 | SSH2_FINGERPRINT_HEX);
        if(strcmp(strtoupper(str_replace(':', '', $this->ssh_server_fp)), $fingerprint) !== 0)
        { 
            throw new Exception(locText('SERVERID_NOK')); 
        }
        $tOutput[] = locText('SERVERID_OK');

        if(!ssh2_auth_pubkey_file($this->connection, $this->ssh_auth_user, $this->ssh_auth_pub, $this->ssh_auth_priv, $this->ssh_auth_pass))
        { 
            throw new Exception(locText('USERLOGIN_NOK', array($ssh_auth_user))); 
        }
        $tOutput[] = locText('USERLOGIN_OK', array($ssh_auth_user));

        return $tOutput;
    } 
    public function exec($cmd)
    {
        $sOutput = '';
        if(!($stream = ssh2_exec($this->connection, $cmd)))
        {
            throw new Exception(locText('COMMANDFAIL'));
        }
        stream_set_blocking($stream, true);
        while($buf = fread($stream, 4096))
        {
            $sOutput .= $buf;
        }
        fclose($stream);
        /*
        $time_start = time();
        while (true){
            $sOutput .= fread($stream, 4096);
            if (strpos($sOutput,"__COMMAND_FINISHED__") !== false) {
                echo "okay: command finished\n";
                break;
            }
            if ((time()-$time_start) > 10 ) {
                echo "fail: timeout of 10 seconds has been reached\n";
                break;
            }
        }
        */
        return $sOutput;
    } 
    public function disconnect()
    {
        $tOutput = array();
        $tOutput[] = $this->exec('exit;');
        $this->connection = null;
        $tOutput[] = locText('EXIT_OK');

        return $tOutput;
    }
}

?>
