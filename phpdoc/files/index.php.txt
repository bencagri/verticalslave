<?php

/**
 * Gateway of VerticalSlave
 * Single entry point for the tool
 *
 * This tool is designed to manage mysql synchronisation between two linux servers.
 *
 * The "master" is the server holding the live environment, whose data will be read.
 * The "slave" is a server designed to receive a copy of the master's data.
 * We use "master" and "slave" terms at large, but in the mysql replication part
 * of this tool, we use "master" and "slave" terms strictly, as designed by Mysql documentation.
 *
 * This tool is designed to work, both on a on-demand basis with an Apache virtualhost,
 * or on a cron job basis.
 *
 * Every remote mysql connection uses SSL. So you need to generate SSL clients certificates 
 * using a certification authority generated on master.
 *
 * This tool works best if you disables PHP output buffering.
 * The function setNoBuffer() tries to set things to do that but it could fail 
 * according to your server configuration.
 *
 * To ensure a basic security level, a custom authentication key is used (defined in config file)
 *
 * Three actions are possible :
 *
 * 1. Reset replication
 * 
 * This action requires a mysql master and a mysql slave fully configured to work together.
 * See mysql documentation and other sources to set up your replication environment.
 *
 * Each replicated database has to be declared on slave with a replicate-do-db instruction in my.cnf.
 *
 * The tool checks the database integrity on master, deletes and recreates the databases on slave,
 * then it dumps all databases with a single mysqldump command piped with a mysql connection to slave.
 * Historically, the mysqldump command was first executed within a SSH connection from slave on master 
 * with a ssh backlink to slave. PHP libssh2 was ok with that, but no output could be caught.
 * The option was kept but is not necessary. Prefer classic mysqldump SSL connection.
 *
 * The tool then starts the replication using coordinates grabbed when dumping and do some checks
 * by reading master and slave status and verify the integrity of the data from both sides.
 *
 *
 * 2. Check replication
 *
 * The tool reads master and slave status and checks integrity of the data on both sides
 *
 *
 * 3. Dump databases
 *
 * For databases you don't want to replicate (dev or test environments for instance),
 * the tool can be used to only dump a list of databases you declare in config.
 *
 *
 *
 * package		VerticalSlave
 * @category	common
 * @author 		Zalem Citizen
 * @version 	0.1.0
 * 
 */

// Paramétrage PHP
ini_set('error_reporting', E_ALL & ~E_NOTICE);
ini_set('max_execution_time', 3600);

// Ressources externes
require_once('config.php'); /** @uses general configuration */
require_once('functions.php'); /** @uses general functions */
require_once('views/locale.'.$tCONFIG['lang'].'.php'); /** @uses locale texts */
setNoBuffer();

sendOutput('<pre>');
if(empty($_GET['authKey']) || $_GET['authKey'] != $tCONFIG['authKey'])
	verticalDie(locText('UNAUTHORIZED'));
else
	sendOutput(locText('AUTHORIZED'), 'vv');

sendOutput(setAsMajor(locText('TOOLTITLE', array($tCONFIG['slaveSshHost']))));

require_once('models/connect.db.php'); /** @uses general mysql connection functions */
require_once('models/functions.db.php'); /** @uses general mysql functions */
require_once('ctrlrs/checkconfig.php'); /** @uses configuration check controller */

sendOutput(setAsTitle(locText('CHOOSEACTION')));
sendOutput(locText('LNK_CHECK', array($_GET['authKey'])));
sendOutput(locText('LNK_RESET', array($_GET['authKey'])));
sendOutput(locText('LNK_DUMP', array($_GET['authKey'])));

switch($_GET['action'])
{
	case 'reset':
		$action = 'reset';
		require_once('ctrlrs/resetreplication.php'); /** @uses replication reset controller */
	break;

	case 'check':
		$action = 'check';
		require_once('ctrlrs/checkreplication.php'); /** @uses replication check controller */
	break;

	case 'dump':
		$action = 'dump';
		require_once('ctrlrs/dump.php'); /** @uses dump controller */
	break;
}

require('models/disconnect.db.php'); /** @uses general mysql disconnect actions */

sendOutput(locText('JOBDONE'));
sendOutput('</pre>');

?>
