<?php

/**
 * Executes some checks on user configuration.
 *
 * package     VerticalSlave
 * @category    controllers
 * @author      ZalemCitizen
 *
 */

sendOutput(setAsTitle(locText('VERIFYCONFIG')));
$bValidConfig = true;

// Verif que l'on est sur le slave
if($tCONFIG['slaveMysqlHost'] != 'localhost')
{
	sendOutput(locText('EXECONSLAVE_NOK'));
	$bValidConfig = false;
}
else
	sendOutput(locText('EXECONSLAVE_OK'), 'vv');

// Vérif type de connexion pour le dump
if($tCONFIG['dumpConnectionType'] == 'ssl')
{
	sendOutput(locText('CONNECTIONTYPE', array('ssl')));
}
else
{
	sendOutput(locText('CONNECTIONTYPE_NOK'));
	$bValidConfig = false;
}

// Dans tous les cas la connexion Mysql au master doit être protégée par SSL
// Vérif de la présence des fichiers SSL
if(!isset($tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_CA]) || empty($tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_CA]))
{
	sendOutput(locText('SSLCA_NOK', array($tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_CA])));
	$bValidConfig = false;
}
else
{
	if(file_exists($tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_CA]))
		sendOutput(locText('SSLCA_OK', array($tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_CA])), 'vv');
	else
	{
		sendOutput(locText('SSLCAFILE_NOK', array($tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_CA])));
		$bValidConfig = false;
	}
}

if(!isset($tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_KEY]) || empty($tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_KEY]))
{
	sendOutput(locText('SSLKEY_NOK', array($tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_KEY])));
	$bValidConfig = false;
}
else
{
	if(file_exists($tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_KEY]))
		sendOutput(locText('SSLKEY_OK', array($tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_KEY])), 'vv');
	else
	{
		sendOutput(locText('SSLKEYFILE_NOK', array($tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_KEY])));
		$bValidConfig = false;
	}
}

if(!isset($tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_CERT]) || empty($tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_CERT]))
{
	sendOutput(locText('SSLCERT_NOK', array($tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_CERT])));
	$bValidConfig = false;
}
else
{
	if(file_exists($tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_CERT]))
		sendOutput(locText('SSLCERT_OK', array($tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_CERT])), 'vv');
	else
	{
		sendOutput(locText('SSLCERTFILE_NOK', array($tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_CERT])));
		$bValidConfig = false;
	}
}

if(!$bValidConfig)
	verticalDie(locText('VERIFYCONFIG_NOK'));

// Connexions
$oMysqlMaster = masterConnect();
$oMysqlSlave = slaveConnect();

// Vérification des droits du user mysql sur SLAVE
$tSlaveUserGrants = getUserGrants($oMysqlSlave, 'SLAVE');
if($tSlaveUserGrants)
{
	$sSlaveUserGrants = str_replace('GRANT ', '', $tSlaveUserGrants[0]);
	$tSlaveUserGrants = explode(', ', substr($sSlaveUserGrants, 0, strpos($sSlaveUserGrants, ' ON ')));
	$tRequiredGrants = array('SELECT', 'INSERT', 'UPDATE', 'CREATE', 'DROP', 'RELOAD', 'INDEX', 'ALTER', 'SHOW DATABASES', 'SUPER', 'LOCK TABLES', 'REPLICATION SLAVE', 'REPLICATION CLIENT', 'TRIGGER', 'SHOW VIEW');

	$tMissingGrants = array();
	foreach($tRequiredGrants AS $k=>$grant)
	{
		if(!in_array($grant, $tSlaveUserGrants))
			$tMissingGrants[] = $grant;
	}

	if(!empty($tMissingGrants))
	{
		sendOutput(locText('VERIFYGRANTS_NOK', array($tCONFIG['slaveMysqlUser'], 'SLAVE', implode(', ', $tMissingGrants))));
		$bValidConfig = false;
	}
	else
		sendOutput(locText('VERIFYGRANTS_OK', array($tCONFIG['slaveMysqlUser'], 'SLAVE')), 'vv');
}
else
	$bValidConfig = false;

// Vérification que le SLAVE est bien configuré en slave
if(!($oSlaveStatus = getSlaveStatus($oMysqlSlave)))
{
	// Si exactement false: PDO exception
	if($oSlaveStatus === false)
		$bValidConfig = false;
	// Sinon, pas de rôle slave mais on peut continuer
	else
	{
		sendOutput(locText('SLAVEROLE_STATUS_NOK'));
		$bNoReplication = true;
	}
}
else
{
	if($oSlaveStatus->Master_Host != $tCONFIG['masterMysqlHost'])
	{
		sendOutput(locText('SLAVEROLE_MASTERDATA_NOK', array('Master_Host', $oSlaveStatus->Master_Host, $tCONFIG['masterMysqlHost'])));
		$bValidConfig = false;
	}
	elseif($oSlaveStatus->Master_Port != $tCONFIG['masterMysqlPort'])
	{
		sendOutput(locText('SLAVEROLE_MASTERDATA_NOK', array('Master_Port', $oSlaveStatus->Master_Port, $tCONFIG['masterMysqlPort'])));
		$bValidConfig = false;
	}
	elseif($oSlaveStatus->Master_User != $tCONFIG['masterMysqlUser'])
	{
		sendOutput(locText('SLAVEROLE_MASTERDATA_NOK', array('Master_User', $oSlaveStatus->Master_User, $tCONFIG['masterMysqlUser'])));
		$bValidConfig = false;
	}
	elseif($oSlaveStatus->Master_SSL_CA_File != $tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_CA])
	{
		sendOutput(locText('SLAVEROLE_MASTERDATA_NOK', array('Master_SSL_CA_File', $oSlaveStatus->Master_SSL_CA_File, $tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_CA])));
		$bValidConfig = false;
	}
	elseif($oSlaveStatus->Master_SSL_Key != $tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_KEY])
	{
		sendOutput(locText('SLAVEROLE_MASTERDATA_NOK', array('Master_SSL_Key', $oSlaveStatus->Master_SSL_Key, $tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_KEY])));
		$bValidConfig = false;
	}
	elseif($oSlaveStatus->Master_SSL_Cert != $tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_CERT])
	{
		sendOutput(locText('SLAVEROLE_MASTERDATA_NOK', array('Master_SSL_Key', $oSlaveStatus->Master_SSL_Cert, $tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_CERT])));
		$bValidConfig = false;
	}
	else
	{
		sendOutput(locText('SLAVEROLE_OK'));
		sendOutput(locText('DBLIST', array($oSlaveStatus->Replicate_Do_DB)));
	}
}

// Vérification des droits du user mysql sur MASTER
$tMasterUserGrants = getUserGrants($oMysqlMaster, 'MASTER');
if($tMasterUserGrants)
{
	$sMasterUserGrants = str_replace('GRANT ', '', $tMasterUserGrants[0]);
	$tMasterUserGrants = explode(', ', substr($sMasterUserGrants, 0, strpos($sMasterUserGrants, ' ON ')));
	$tRequiredGrants = array('SELECT', 'RELOAD', 'SUPER', 'REPLICATION SLAVE');

	$tMissingGrants = array();
	foreach($tRequiredGrants AS $k=>$grant)
	{
		if(!in_array($grant, $tMasterUserGrants))
			$tMissingGrants[] = $grant;
	}

	if(!empty($tMissingGrants))
	{
		sendOutput(locText('VERIFYGRANTS_NOK', array($tCONFIG['masterMysqlUser'], 'MASTER', implode(', ', $tMissingGrants))));
		$bValidConfig = false;
	}
	else
		sendOutput(locText('VERIFYGRANTS_OK', array($tCONFIG['masterMysqlUser']), 'MASTER'), 'vv');
}
else
	$bValidConfig = false;

// Vérification que le MASTER est bien cofiguré en master
if(!($oMasterStatus = getMasterStatus($oMysqlMaster)))
{
	if($oMasterStatus === false)
		$bValidConfig = false;
	else
	{
		sendOutput(locText('MASTERROLE_STATUS_NOK'));
		$bNoReplication = true;
	}
}
else
{
	sendOutput(locText('MASTERROLE_OK'));
}

if($bValidConfig)
	sendOutput(locText('VERIFYCONFIG_OK'));
else
	verticalDie(locText('VERIFYCONFIG_NOK'));

?>