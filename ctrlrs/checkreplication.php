<?php

/**
 * Checks running replication
 *
 * package     VerticalSlave
 * @category    controllers
 * @author      ZalemCitizen
 *
 */

if($action != 'check')
	verticalDie(locText('UNAUTHORIZED'));

sendOutput(setAsMajor(locText('TITLE_CHECK', array($tCONFIG['slaveSshHost']))));

sendOutput(setAsTitle(locText('PREPARE')));

if(!($oMysqlMaster instanceof PDO && $oMysqlSlave instanceof PDO))
{
	$oMysqlMaster = masterConnect();
	$oMysqlSlave = slaveConnect();
}

// Master status
if(!($oMasterStatus = getMasterStatus($oMysqlMaster)))
{
	verticalDie();
}

$indent++;
foreach($oMasterStatus AS $field=>$value)
	sendOutput(' > '.str_pad($field, 30).":\t".$value, 'vvv');
$indent = min0($indent-1);


// Slave Status
if(!($oSlaveStatus = getSlaveStatus($oMysqlSlave)))
{
	verticalDie();
}

$indent++;
foreach($oSlaveStatus AS $field=>$value)
	sendOutput(' > '.str_pad($field, 30).":\t".$value, 'vvv');
$indent = min0($indent-1);

sendOutput(setAsTitle(locText('VERIFYSTATUS')));

$bCheckError = false;
// Le slave est-il en erreur ?
if($oSlaveStatus->Errno != 0 || $oSlaveStatus->Error != '')
{
	sendOutput(locText('ERRORREPORT_NOK'));
	sendOutput(locText('ERRORDETAILS', array($oSlaveStatus->Errno, $oSlaveStatus->Error)));
	$bCheckError = true;
}
else
	sendOutput(locText('ERRORREPORT_OK'), 'vv');

// Les processus du slave sont-ils démarrés ?
if($oSlaveStatus->Slave_IO_Running != 'Yes')
{
	sendOutput(locText('IOTHREADEXEC_NOK'));
	$bCheckError = true;
}
else
	sendOutput(locText('IOTHREADEXEC_OK'), 'vv');

if($oSlaveStatus->Slave_SQL_Running != 'Yes')
{
	sendOutput(locText('SQLTHREADEXEC_NOK'));
	$bCheckError = true;
}
else
	sendOutput(locText('SQLTHREADEXEC_OK'), 'vv');

// La position de lecture du master sur le slave est-elle la meme que la position d'écriture du master ?
if($oSlaveStatus->Master_Log_File != $oMasterStatus->File || $oSlaveStatus->Read_Master_Log_Pos != $oMasterStatus->Position)
{
	sendOutput(locText('READPOS_NOK', array($oSlaveStatus->Read_Master_Log_Pos, $oSlaveStatus->Master_Log_File, $oMasterStatus->Position, $oMasterStatus->File)));
	$bCheckError = true;
}
else
	sendOutput(locText('READPOS_OK', array($oSlaveStatus->Read_Master_Log_Pos, $oSlaveStatus->Master_Log_File)), 'vv');

// La position d'exécution du slave est elle la même que la position de lecture ?
if($oSlaveStatus->Exec_Master_Log_Pos != $oSlaveStatus->Read_Master_Log_Pos)
{
	sendOutput(locText('EXECPOS_NOK', array($oSlaveStatus->Read_Master_Log_Pos, $oSlaveStatus->Exec_Master_Log_Pos)));
	$bCheckError = true;
}
else
	sendOutput(locText('EXECPOS_OK', array($oSlaveStatus->Exec_Master_Log_Pos)), 'vv');

// Secondes derrière le master
if($oSlaveStatus->Seconds_Behind_Master > 0)
{
	sendOutput(locText('SYNCHRO_NOK', array($oSlaveStatus->Seconds_Behind_Master)));
	$bCheckError = true;
}
else
	sendOutput(locText('SYNCHRO_OK', array($oSlaveStatus->Seconds_Behind_Master)));

// Terminaison de la vérif des statuts
if(!$bCheckError)
	sendOutput(locText('VERIFYSTATUS_OK'));
else
	sendOutput(locText('VERIFYSTATUS_NOK'));

if(!$bDontCompareChecksum)
{
	sendOutput(setAsTitle(locText('VERIFYDATA')));
	// Les sommes de contrôles sont elles identiques ?
	$tDBlist = explode(',', $oSlaveStatus->Replicate_Do_DB);

	$indent++;
	foreach($tDBlist AS $k=>$db)
	{
		require('ctrlrs/checkdb.php'); /** @uses database verifications */
	}
	$indent = min0($indent-1);

	// Terminaison de la vérif des données
	if(!$bCheckError)
		sendOutput(locText('VERIFYDATA_OK'));
	else
		sendOutput(locText('VERIFYDATA_NOK'));
}


// Terminaison de la verif
if(!$bCheckError)
	sendOutput(locText('VERIFYREPL_OK'));
else
	sendOutput(locText('VERIFYREPL_NOK'));

?>