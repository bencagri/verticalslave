<?php

/**
 * Executes some checks on database $db.
 *
 * package     VerticalSlave
 * @category    controllers
 * @author      ZalemCitizen
 *
 */

if(!isset($action))
	verticalDie(locText('UNAUTHORIZED'));

if(!($oMysqlMaster instanceof PDO && $oMysqlSlave instanceof PDO))
{
	sendOutput(locText('CHECKDB_DBCONNECTIONS_NOK'));
	$bCheckError = true;
	return;
}

if(!isset($db))
{
	sendOutput(locText('CHECKDB_DBUNDEFINED'));
	$bCheckError = true;
	return;
}

sendOutput(setAsTitle(locText('DBNAME', array(($k+1), $db))));

// Show table status Master
$tMasterTablesStatus = getTablesStatus($oMysqlMaster, $db, 'MASTER');
$tSlaveTablesStatus = getTablesStatus($oMysqlSlave, $db, 'SLAVE');

if(!$tMasterTablesStatus || !$tSlaveTablesStatus)
{
	$bStopLaunch = true;
}

$indent++;
$iCheckError = 2;
$bCheckError = false;
foreach($tMasterTablesStatus AS $sTableName=>$tTableStatus)
{
	$iCheckErrorThisTable = 2;
	$bCheckErrorThisTable = false;
	sendOutput(locText('VERIFYTABLE', array($sTableName)), 'vv');
	$indent++;

	// Table status
	if($tMasterTablesStatus[$sTableName]['Rows'] == $tSlaveTablesStatus[$sTableName]['Rows'])
		sendOutput(locText('VERIFYTABLENB_OK'), 'vv');
	else
	{
		sendOutput(locText('VERIFYTABLENB_NOK'), 'vv');
		$bCheckErrorThisTable = true;
	}

	// CHECK TABLE sur SLAVE
	$iCheckErrorThisTable = getCheckTable($oMysqlMaster, $db, $sTableName);
	if($iCheckErrorThisTable == 0)
		$bCheckErrorThisTable = true;

	// Comparaison checksum des tables MASTER/SLAVE
	if($tMasterTablesStatus[$sTableName]['Rows'] > 0 && $tMasterTablesStatus[$sTableName]['Rows'] == $tSlaveTablesStatus[$sTableName]['Rows'])
	{
		if(!getChecksumCompare($oMysqlMaster, $oMysqlSlave, $db, $sTableName))
			$bCheckErrorThisTable = true;
	}
	elseif($tMasterTablesStatus[$sTableName]['Rows'] == 0 && $tMasterTablesStatus[$sTableName]['Rows'] == $tSlaveTablesStatus[$sTableName]['Rows'])
		sendOutput(locText('VERIFYCHECKSUM_OKEMPTY', array($sTableName)), 'vvv');
	else
		sendOutput(locText('VERIFYCHECKSUM_NOK', array($sTableName)), 'vvv');

	$indent = min0($indent-1);
	if($bCheckErrorThisTable)
		sendOutput(locText('VERIFYTABLE_NOK', array($sTableName)), 'vv');
	else
		sendOutput(locText('VERIFYTABLE_OK', array($sTableName)), 'vv');

	$iCheckError = $iCheckErrorThisTable;
	$bCheckError = $bCheckErrorThisTable;
}

$indent = min0($indent-1);
if(!$bCheckError)
{
	if($iCheckError == 1)
		sendOutput(locText('VERIFYDBWARNING'));
	else
		sendOutput(locText('VERIFYDB_OK'));
}
else
{
	sendOutput(locText('VERIFYDB_NOK'));
	$bStopLaunch = true;
}

?>