<?php

/**
 * Dumps a list of databases defined in $tCONFIG. @see /config.default.php
 *
 * package     VerticalSlave
 * @category    controllers
 * @author      ZalemCitizen
 *
 */

if($action != 'dump')
	verticalDie(locText('UNAUTHORIZED'));

// Application
sendOutput(setAsMajor(locText('TITLE_DUMP', array($tCONFIG['slaveSshHost']))));

if($_GET['cmd'] != 'launch')
{
	// Avertissement arrêt de la tâche CRON de contrôle de réplication
	sendOutput(locText('DSCL_HOUR'));
	if($tCONFIG['dumpConnectionType'] == 'ssh2')
		sendOutput(locText('DSCL_SHAREKEYS'));
	elseif($tCONFIG['dumpConnectionType'] == 'ssl')
		sendOutput(locText('DSCL_SSLFILES'));

	if(!isset($tCONFIG['dumpDatabases']) || empty($tCONFIG['dumpDatabases']))
		verticalDie(locText('DUMPDBLIST_NONE'));

	sendOutput(locText('DUMPDBLIST', array(implode(', ', $tCONFIG['dumpDatabases']))));
	sendOutput(locText('LNK_LAUNCH', array($_GET['authKey'], 'dump')));
}
else
{
	sendOutput(setAsTitle('Préparation'));

	if(!($oMysqlMaster instanceof PDO && $oMysqlSlave instanceof PDO))
	{
		$oMysqlMaster = masterConnect();
		$oMysqlSlave = slaveConnect();
	}

	$bStartLaunch = false;

	// Flush des tables
	if(!setFlushTables($oMysqlMaster))
	{
		verticalDie();
	}

	sendOutput(setAsTitle(locText('VERIFYBEFOREDUMP')));

	if(!isset($tCONFIG['dumpDatabases']) || empty($tCONFIG['dumpDatabases']))
		verticalDie(locText('DUMPDBLIST_NONE'));

	sendOutput(locText('DUMPDBLIST', array(implode(', ', $tCONFIG['dumpDatabases']))));
	$tDBlist = $tCONFIG['dumpDatabases'];

	$indent++;

	// Pour chaque base
	foreach($tDBlist AS $k=>$db)
	{
		sendOutput(locText('DBNAME', array(($k+1), $db)));

		// Liste des tables sur MASTER
		if(!$oMasterTableList = getTableList($oMysqlMaster, $db, 'MASTER'))
		{
			continue; // On passe à la base suivante
		}

		// Pour chaque table
		$iCheckError = 2;
		sendOutput(locText('TABLEINTEGRITY', array('MASTER')));
		$indent++;
		while($sTableName = $oMasterTableList->fetchColumn())
		{
			// CHECK TABLE sur MASTER
			$iCheckError = getCheckTable($oMysqlMaster, $db, $sTableName);
			if($iCheckError === 0)
				continue;
		}

		$indent = min0($indent-1);
		if($iCheckError == 2)
			sendOutput(locText('TABLEINTEGRITY_OK'));
		elseif($iCheckError == 1)
			sendOutput(locText('OTHERTABLEINTEGRITY_OK'));
		else
		{
			sendOutput(locText('TABLEINTEGRITY_NOK'));
			continue;
		}

		// Suppression de la base sur SLAVE
		if(!setDropDatabase($oMysqlSlave, $db, 'SLAVE'))
		{
		    continue;
		}

		// Création de la base
		if(!setCreateDatabase($oMysqlSlave, $db, 'SLAVE'))
		{
		    continue;
		}
		$bStartLaunch = true;
	}
	$indent = min0($indent-1);

	// Si tout est ok, on lance le dump
	if(!$bStartLaunch)
		verticalDie(locText('VERIFYBEFOREDUMP_NOK'));
	else
	{
		sendOutput(locText('VERIFYBEFOREDUMP_OK'));

		if($tCONFIG['dumpConnectionType'] == 'ssl')
		{
			sendOutput(setAsTitle(locText('DUMPUSINGSSL')));

			// Dump du contenu de la base: tunnel SSH ouvert entre localhost et MASTER
			// La commande établie un tunnel ssh de localhost au MASTER
			// par ce tunnel elle exécute mysqldump de la base suivi d'un tunnel entre MASTER et SLAVE pour ouvrir une connexion mysql recevant la sortie de mysqldump
			// le STDOUT de la commande mysqldump pipée dans ssh ne ressort pas
			sendOutput(locText('DUMPING'));
			try
			{
				// Remote dump
				// --opt: équivalent à --add-drop-table --add-locks --create-options --disable-keys --extended-insert --lock-tables --quick --set-charset
				// --lock-all-tables: (implicite avec --master-data) applique un verrou global READ sur toutes les tables jusqu'à la fin du dump (mise en attente des écritures concurrentes)
				// --master-data=1: écrit la position du MASTER dans une requête CHANGE MASTER pour automatiquement positionner le SLAVE
				// --compress: active la compression entre serveur et client
				// -v: verbose
				// http://dev.mysql.com/doc/refman/5.1/en/mysqldump.html
				// Cette commande renvoie le dump dans STDOUT et les infos et erreurs du mode verbeux dans STDERR
				// http://stackoverflow.com/questions/7645499/getting-the-real-exit-code-after-proc-open
				// http://tldp.org/LDP/abs/html/internalvariables.html
				// http://php.net//manual/fr/function.proc-open.php
				// http://stackoverflow.com/questions/1101957/are-there-any-standard-exit-status-codes-in-linux
				// http://www.tldp.org/LDP/abs/html/exitcodes.html
				if($tCONFIG['verbose'] != 'v')
					$v = '-v';
				$cmd = "/usr/bin/mysqldump ".$v." --opt --lock-all-tables --host=".$tCONFIG['masterMysqlHost']." --port=".$tCONFIG['masterMysqlPort']." --ssl=1 --ssl-ca=".$tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_CA]." --ssl-key=".$tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_KEY]." --ssl-cert=".$tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_CERT]." -u <masterMysqlUser> -p'<masterMysqlPwd>' --databases ".implode(" ", $tDBlist)." | /usr/bin/mysql -u <slaveMysqlUser> -p'<slaveMysqlPwd>'";//;echo \${PIPESTATUS[0]}";

				$indent++;
				sendOutput(locText('DUMPCOMMAND', array($cmd)), 'vv');
				
				// We ad credentials here, so that output does not contain user and password (especially if sent by mail by CRON output)
				$cmd = str_replace('<masterMysqlUser>', $tCONFIG['masterMysqlUser'], $cmd);
				$cmd = str_replace('<masterMysqlPwd>', $tCONFIG['masterMysqlPwd'], $cmd);
				$cmd = str_replace('<slaveMysqlUser>', $tCONFIG['slaveMysqlUser'], $cmd);
				$cmd = str_replace('<slaveMysqlPwd>', $tCONFIG['slaveMysqlPwd'], $cmd);

				$descriptorspec = array(
				   0 => array("pipe", "r"),   // STDIN is a pipe that the child will read from
				   1 => array("pipe", "w"),   // STDOUT is a pipe that the child will write to
				   2 => array("pipe", "w")    // STDERR is a pipe that the child will write to
				);

				$process = proc_open($cmd, $descriptorspec, $pipes, realpath('./'));
				if(is_resource($process))
				{
				    while($s = fgets($pipes[2]))
				    {
				        sendOutput($s, 'vv', '');
				    }
				    fclose($pipes[2]);
				}

				/*
				 * Problème: exitcode est le code de sortie de la commande située après le pipe |
				 * La variable interne $PIPESTATUS qui stocke la code de sortie de chaque commande du pipe semble inaccessible pour PHP
				 * exitcode est donc inutile en l'état
				 */
				/*
				$i = 0;
				$tStatus = proc_get_status($process);
				while($tStatus["running"] && $i<10)
				{
					sleep(1);
					$tStatus = proc_get_status($process);
					$i++;
				}

				if($tStatus['running'] === false)
					sendOutput('Code de sortie: '.$tStatus['exitcode']);
				*/

				proc_close($process);

				$indent = min0($indent-1);
				sendOutput(locText('DUMPINGOVER'));
			}
			catch(Exception $e)
			{
			    $bStopLaunch = true;
			    verticalDie(locText('ERRORSSL', array($e->getMessage())));
			}
		}
		else
			verticalDie(locText('CONNECTIONTYPE_NOK'));
	}

	// Vérification de la copie
	sendOutput(setAsTitle(locText('VERIFYAFTERDUMP')));
	$indent++;
	foreach($tDBlist AS $k=>$db)
	{
		require('ctrlrs/checkdb.php'); /** @uses database verifications */
	}
	$indent = min0($indent-1);

	// Les eventuelles erreurs précédentes empêchent la suite
	if($bStopLaunch)
	{
		verticalDie(locText('VERIFYAFTERDUMP_NOK'));
	}
	else
	{
		sendOutput(locText('VERIFYAFTERDUMP_OK'));

		// Check Replication
		$action = 'check';
		$bDontCompareChecksum = true; // Inutile car vient d'être fait
		require('ctrlrs/checkreplication.php'); /** @uses replication verifications */

		sendOutput(locText('DUMP_OK'));
	}
}

?>