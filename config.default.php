<?php

/**
 * Configuration
 * Defines settings for VerticalSlave application
 *
 * package		VerticalSlave
 * @category	common
 * @author 		ZalemCitizen
 * 
 * @global		array $tCONFIG array of configuration parameters
 */

$tCONFIG = array(
	/** @internal Master's mysql parameters */
	'masterMysqlHost'		=>	'', /** @var string Your mysql master server hostname */
	'masterMysqlPort'		=>	3306, /** @var integer Your mysql master server custom port. Ex: 3306 */
	'masterMysqlUser'		=>	'', /** @var string Your mysql master replication user */
	'masterMysqlPwd'		=>	'', /** @var string Your mysql master replication user password */
	'masterPdoSettings'		=>	array(
	    PDO::MYSQL_ATTR_SSL_CA   => '', /** @var string The absolute path to CA file generated on master server and installed at that path on slave server */
	    PDO::MYSQL_ATTR_SSL_KEY  => '', /** @var string The absolute path to client private key file generated on master server and installed at that path on slave server */
	    PDO::MYSQL_ATTR_SSL_CERT => '', /** @var string The absolute path to client certificate file generated on master server and installed at that path on slave server */
	    PDO::ATTR_ERRMODE		 =>	PDO::ERRMODE_EXCEPTION, /** @var object The PDO constant to manage PDO errors on master mysql connection, @see http://php.net/manual/en/pdo.error-handling.php */
    ),
    /** @internal Slave's mysql parameters */
	'slaveMysqlHost'		=>	'', /** @var string Your mysql slave server hostname. This tool should work outside of slave with slight modifications but this hasn't been tested. Execute this tool from your slave instead  */
	'slaveMysqlPort'		=>	3306, /** @var integer Your mysql slave server custom port. Ex: 3306 */
	'slaveMysqlUser'		=>	'', /** @var string Your mysql slave replication user. This user should be granted with SELECT INSERT UPDATE CREATE ALTER INDEX DROP SHOW_VIEW TRIGGER SUPER RELOAD SHOW_DATABASE LOCK TABLES REPLICATION_CLIENT REPLICATION_SLAVE */
	'slaveMysqlPwd'			=>	'', /** @var string Your mysql master replication user password */
	'slavePdoSettings'		=>	array(
	    PDO::ATTR_ERRMODE		 =>	PDO::ERRMODE_EXCEPTION, /** @var object The PDO constant to manage PDO errors on slave mysql connection, @see http://php.net/manual/en/pdo.error-handling.php */
    ),
	/** @internal General settings */
    'lang'					=>	'enu', /** @var string The language of output. Must match a locale.lng.php file */
    'checkTableMode'		=>	'MEDIUM', /** @var string The data consistency option for CHECK TABLE queries. Can be set to {QUICK | FAST | MEDIUM | EXTENDED | CHANGED}  @see http://dev.mysql.com/doc/refman/5.5/en/check-table.html */
    'checksumTableMode'		=>	'EXTENDED', /** @var string The option for CHECKSUM TABLE queries. Can be set to {QUICK | EXTENDED}. @see http://dev.mysql.com/doc/refman/5.5/en/checksum-table.html */
	'sleepDuration'			=>	5, /** @var string The duration of pauses in seconds when using sleep(). Used especially after START SLAVE */
	'verbose'				=>	'vvv', /** @var string The level of details you want from verbose mode. This can be set to {v | vv | vvv}. */
	'authKey'				=>	'224F1sc9zKx7PzMnc', /** @var string The level of details you want from verbose mode. This can be set to {v | vv | vvv}. */
	'dumpConnectionType'	=>	'ssl', /** @var string The type of connection you establish to dump the databases. ssl is only supported now. */
	'dumpDatabases'			=>	array(
		'',
	), /** @var array The list of non-replicated databases you want to dump */
);

?>