# **VerticalSlave** #

This utility provides features to manage a simple mysql replication environment.

## Description ##

The tool is designed to give a simple way to keep a mysql replication active. It has 3 features :

* Check the status of running replication
* Reset replication
* Dump databases

It can easily be run within a browser or using CRON job.

## Installation ##

### Pre-requisites ###

Difficulty: 2/5
This tool requires from you to:
* understand how mysql replication works, understand the fields shown when querying master and slave status and how to configure slave and master servers
* know how to establish mysql connection over ssl (generate the needed files)
* know a bit about php and how to set up virtualhost in apache

This script requires 2 linux computers each hosting a MySQL server 5.0 or higher (tested and used on 5.5, though):

* one is configured as MASTER
* other is configured as SLAVE

The tool has to run on the slave so slave needs Apache 2.2 or higher and PHP 5.3 or higher with PDO extension.
User to connect to master needs following mysql grants: 
User to connect to slave needs following mysql grants: 

Mysql connection to slave is done locally.
Mysql connection to master is needing SSL. Use mysql documentation to generate SSL files (CA, client private key and client certificate) to encrypt your data transfers.

The tool can work outside of a mysql replication environment to dump a list of databases you define in config.
In that case, consider "MASTER" as the source server with data we want to read/get and "SLAVE" as the destination server, where data will be written/put.

The tool was designed for databases using MyISAM engine. InnoDB databases can be handled better with a few improvements (FLUSH TABLES queries are not well adapted for such databases).

The tool tries to set up its PHP environement to stop buffering. Some `ini_set` instructions are thrown to do so and let you have a real-time output.
The reset and dump features use the PHP function exec to work.

### Setting up ###

* Download or clone this repository in the directory of your choice on your slave server. Don't forget x (execute) permission on *.php files
* Configure an apache virtualhost to give access to the tool via HTTPS (recommanded)
* Copy config.default.php to config.php and edit the config fields you need
* make sure SSL files to mysql connect to master are usable
* make sure your replication settings are correctly set up (use mysql doc or tutorials to do so)
* access the tol using https://your-hostname-or-ip/?auth=your-auth-key-defined-in-config.php

### Configuration ###

You need to copy config.default.php to config.php before using this tool.
This file contains all needed parameters :

#### Mysql parameters ####

##### For master #####

* masterMysqlHost: the hostname or ip used to establish the mysql connection to your master server
* masterMysqlPort: the port used to establish the mysql connection to your master server. Default's to 3306, but I recommend non standard port (configure your custom port in your `/etc/mysql/my.cnf`
* masterMysqlUser: the user to authenticate on mysql master server
* masterMysqlPwd: the password to authenticate on mysql master server
* masterPdoSettings: the various PDO options you want to use. The tool requires you to use at least the following options:
    * PDO::MYSQL_ATTR_SSL_CA: the absolute path to your SSL CA file generated on master and installed on slave (usually in `/etc/mysql/ssl`). Self-signed certification is ok
    * PDO::MYSQL_ATTR_SSL_KEY: the absolute path to your SSL client private key file using CA file when generated on master and installed on slave
    * PDO::MYSQL_ATTR_SSL_CERT: the absolute path to your SSL client certificate file using CA file when generated on master and installed on slave
    * PDO::ATTR_ERRMODE: your desired error mode support for PDO class (default's to PDO::ERRMODE_EXCEPTION)

##### For slave #####

* slaveMysqlHost: hostname or ip to establish mysql connection to slave. Should be left to default (localhost). This tool hasn't be tested when hosted outside of slave
* slaveMysqlPort: the port used to establish the mysql connection to your slave server. Default's to 3306, but I recommend non standard port (configure your custom port in your `/etc/mysql/my.cnf`
* slaveMysqlUser: the user to authenticate on mysql slave server
* slaveMysqlPwd:  the password to authenticate on mysql slave server
* slavePdoSettings: the various PDO options you want to use. The tool requires you to use at least the following options:
    * PDO::ATTR_ERRMODE: your desired error mode support for PDO class (default's to PDO::ERRMODE_EXCEPTION)

#### General settings ####

* lang: the language you want to use for output. Currently supported languages: enu for english/us and fre for french
* checkTableMode: level of details used when doing CHECK TABLE queries. See [mysql doc](http://dev.mysql.com/doc/refman/5.5/en/check-table.html)
* checksumTableMode: depth of calculation used when doing CHECKSUM TABLE queries. See [mysql doc](http://dev.mysql.com/doc/refman/5.5/en/checksum-table.html)
* sleepDuration: The duration of pause used by PHP (currently only used after SLAVE START query)
* verbose: the level of verbose mode you want for output. Default's to max level when empty. Can be set to `v`, `vv` or `vvv`
* authKey: provides some security access to your tool. If set to non empty string, the tool cannot work if authKey is not provided in GET parameters in URL
* dumpConnectionType: the connection type you want to use for mysqldump commands. ssh2 was historically available (and is no more).
It appeared during dev that this way was a bit twisted and unable to catch STDOUT from the double SSH pipe.
The better way is using `ssl` connection type to establish a simple mysqldump over SSL from slave to master and catch the STDOUT to inject it in a local mysql connection to slave.
* dumpDatabases: an array of databases you want to copy with dump feature of the script. Can apply non-replicated databases. In a future version of application, the dump feature will skip the databases you could add here but that are also configured to be replicated, to ensure you are not blowing away your replication environment.

## Operation ##

Each time you reload the tool in your browser, it executes a number a checks on your essential configuration parameters:

* slave defined to localhost ?
* availability of SSL CA, SSL private key and SSL certificate files and mysql connection try using them
* check of required grants for mysql slave and master user
* master status query
* slave status query
* list of replicated databases

### Check replication feature ###

The tool makes 2 kinds of verifications to check the replication.

#### Status check ####

First it queries slave and master about their status.
* Is slave status showing error ?
* Are slave replication threads running ?
* Are slave read position and master write position equal ?
* Are slave exec position and slave read position equal ?
* Are slave and master synchronous (Seconds_Behind_Master is 0) ?

If one of these checks is negative, an error is thrown in output and the second check isn't done.

#### Data check ####

Then, to ensure the integrity of databases is kept, we need to know if the data are the identical.
To do so, we loop on tables list for each replicated databases:

* Is the number of rows equals in slave and master for this table ?
* Is no error nor warning returned when quering with CHECK TABLE ?
* Is the checksum of the table equals on slave and master ?

If any of these checks is negative, an error is thrown in output.

### Reset replication feature ###

This feature is designed to help you to start or restart with a clean replication environment.

#### Clean slave ####

The script first prepare the slave server to get a copy of data from master.
It drops each replicated database it has found in slave status in the `Replicate_Do_Db field`.
It then reacreates each database.

It checks also table integrity on master before going.

#### Dump ####

Then, the dump is launched over SSL with a command like this:

`/usr/bin/mysqldump -v --opt --master-data=1 --lock-all-tables --host=<masterMysqlHost> --port=<masterMysqlPort> --ssl=1 --ssl-ca=<masterPdoSettings[PDO::MYSQL_ATTR_SSL_CA]> --ssl-key=<masterPdoSettings[PDO::MYSQL_ATTR_SSL_KEY]> --ssl-cert=<masterPdoSettings[PDO::MYSQL_ATTR_SSL_CERT]> -u <masterMysqlUser> -p'<masterMysqlPwd>' --databases <database names, space separated> | /usr/bin/mysql -u <slaveMysqlUser> -p'<slaveMysqlPwd>'`

That command uses a pipe to redirect STDOUT of mysqldump (remote opened on master) to opened local mysql connection on slave. STDOUT of mysqldump are mysql ordered instructions to create structure (database, tables, columns, other) and insert data into them.

* -v: if the tool is used in verbose mode, we want the running command to send output through STDERR. We grab the STDERR pipe and display it
* --opt: well known standard dump option, same as:  --add-drop-table, --add-locks, --create-options, --disable-keys, --extended-insert, --lock-tables, --quick, and --set-charset
* --master-data=1: asks mysqldump to write `CHANGE MASTER TO` statements as queries that are used to make the slave start using the right position in master binary logs. Note that master-data=2 returns the same in comment manner to let you do `CHANGE MASTER TO` manually.
* --lock-all-tables: this is actually implicit in --master-data, but left here to remind that --opt only locks the tables during the dump of the database they're in. To be sure the master data used to position the slave are ok, we lock all tables of all databases for the whole process to finish (this was tested).
* --host and --port: trivial, this is how we remote connect to master
* --ssl=1, --ssl-ca, --ssl-key and --ssl-cert: this is how we ensure data is encrypted when transiting on the media
* -u and -p: trivial too, user and password
* --databases: we limit the dump to all databases whose name are specified after this option, separated with spaces
* |: doing the pipe (simultaneous command actually)
* /usr/bin/mysql...: a simple connection to local mysql server (slave)

#### Check dump integrity ####

Finally, the script starts the slave then does checks to verify the replication is ok and dumped data are the same on slave and master.
These checks are basically the same that are down in check feature.

### Dump feature ###

The dump feature is based on the content PHP array defined in config.php.
Each value of the array is a database name dumped by this feature.
Usually you don't want the replicated databases being dumped like this, the reset feature is designed for them.

This script works as reset feature, except the mysqldump command does not get option --master-data.

## Changelog ##

v1.0.0 Initial public release. 3 features: check replication, reset replication, dump databases

## Disclaimer ##

This tool has been tested on Ubuntu 12.04 LTS, Apache 2.2, PHP 5.3, MySQL 5.5 on databases storing approximatively 1M rows max. This hasn't been tested and designed for huge databases with complex replication schema.
I won't take any reponsibility in any data loss or security failure that could occur using it.
You have the charge to check the doings of this tool and to ensure your servers won't be harmed using it.

## Licence ##

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.