<?php

/**
 * Database connections functions
 *
 * package     VerticalSlave
 * @category    models
 * @author      ZalemCitizen
 *
 */

/**
 * masterConnect
 *
 * Opens a mysql connection to master server.
 *
 * @global  array   $tCONFIG    General configuration table. @see /config.default.php
 *
 * @return  object  PDO statement object
 */
function masterConnect()
{
    global $tCONFIG;

    try
    {
        $oMysqlMaster = new PDO('mysql:host='.$tCONFIG['masterMysqlHost'].';port='.$tCONFIG['masterMysqlPort'].';', $tCONFIG['masterMysqlUser'], $tCONFIG['masterMysqlPwd'], $tCONFIG['masterPdoSettings']);
    	sendOutput(locText('DBCONNECT_OK', array('MASTER', $tCONFIG['masterMysqlHost'], $tCONFIG['masterMysqlPort'])));
        return $oMysqlMaster;
    }
    catch(PDOException $e)
    {
        sendOutput(locText('DBCONNECT_NOK', array('MASTER', $tCONFIG['masterMysqlHost'], $tCONFIG['masterMysqlPort'])));
        verticalDie(locText('PDOERROR', array($e->getFile(), $e->getLine(), $e->getMessage())));
    }
}

/**
 * slaveConnect
 *
 * Opens a mysql connection to slave server.
 *
 * @global  array   $tCONFIG    General configuration table. @see /config.default.php
 *
 * @return  object  PDO statement object
 */
function slaveConnect()
{
    global $tCONFIG;

    try
    {
        $oMysqlSlave = new PDO('mysql:host='.$tCONFIG['slaveMysqlHost'].';port='.$tCONFIG['slaveMysqlPort'].';', $tCONFIG['slaveMysqlUser'], $tCONFIG['slaveMysqlPwd'], $tCONFIG['slavePdoSettings']);
        sendOutput(locText('DBCONNECT_OK', array('SLAVE', $tCONFIG['slaveMysqlHost'], $tCONFIG['slaveMysqlPort'])));
        return $oMysqlSlave;
    }
    catch(PDOException $e)
    {
        sendOutput(locText('DBCONNECT_NOK', array('SLAVE', $tCONFIG['slaveMysqlHost'], $tCONFIG['slaveMysqlPort'])));
        verticalDie(locText('PDOERROR', array($e->getFile(), $e->getLine(), $e->getMessage())));
    }
}

?>