<?php

/**
 * Functions used to launch queries through mysql connections provided.
 *
 * package     VerticalSlave
 * @category    models
 * @author      ZalemCitizen
 *
 */

/**
 * getUserGrants
 *
 * Uses provided PDO statement object to grab grants of current user.
 *
 * @param	object	$oMysql		PDO statement object.
 * @param	string	$server		The server name to use in output (Master or SLAVE).
 * @global	array 	$tCONFIG	Global configuration parameters. @see /config.default.php
 *
 * @return	array 	Array of columns from mysql query.
 * @return	bool	False if a PDO exception occurs.
 */
function getUserGrants($oMysql, $server)
{
	global $tCONFIG;

	try
	{
		$tUserGrants = $oMysql->query("SHOW GRANTS FOR CURRENT_USER;", PDO::FETCH_NUM)->fetch();
		return $tUserGrants;
	}
	catch(PDOException $e)
	{
		sendOutput(locText('SHOWGRANTS_NOK', array($tCONFIG[strtolower($server).'MysqlUser'], $server)));
		sendOutput(locText('PDOERROR', array($e->getFile(), $e->getLine(), $e->getMessage())));
		return false;
	}
}

/**
 * setFlushTables
 *
 * Flushes tables on mysql server through mysql connection provided.
 * This ensure a better execution of mysqldump command that follows.
 *
 * @param	object	$oMysql		PDO statement object.
 * @global	array 	$tCONFIG	Global configuration parameters. @see /config.default.php
 *
 * @return	bool	False if a PDO exception occurs. True if none.
 */
function setFlushTables($oMysql)
{
	global $tCONFIG;
	// On flushe les tables et on place un verrou READ
	// Note: Le flushe attend que les requêtes précédentes soient terminées
	// Note: si une requete WRITE arrive, tout ce qui suivra (même en READ seulement) sera bloqué
	sendOutput(locText('FLUSHTABLES'));
	try
	{
		// Attention Flush Table doit attendre la fin des requêtes en cours ou dans le buffer (MyISAM): ça peut etre long
		$oMysql->exec("FLUSH TABLES"); // Note: cette requête n'est pas inscrite dans le log binaire donc pas répliquée
		sendOutput(locText('FLUSHTABLES_OK'));
		return true;
	}
	catch(PDOException $e)
	{
		sendOutput(locText('FLUSHTABLES_NOK'));
		sendOutput(locText('PDOERROR', array($e->getFile(), $e->getLine(), $e->getMessage())));
		return false;
	}
}

/**
 * getTableList
 *
 * Get the list of tables within database provided through mysql connection provided.
 *
 * @param	object	$oMysql		PDO statement object.
 * @param	string	$db			Database name.
 * @param	string	$server		Name of server to add in output message.
 * @global	array 	$tCONFIG	Global configuration parameters. @see /config.default.php
 *
 * @return	array 	Array of table names.
 * @return	bool	False if a PDO exception occurs.
 */
function getTableList($oMysql, $db, $server)
{
	global $tCONFIG;

	try
	{
		$oTableList = $oMysql->query("SHOW TABLES FROM ".$db);
		return $oTableList;
	}
	catch(PDOException $e)
	{
		sendOutput(locText('SHOWTABLES_NOK', array($server)));
		sendOutput(locText('PDOERROR', array($e->getFile(), $e->getLine(), $e->getMessage())));
		return false;
	}
}

/**
 * getTablesStatus
 *
 * Get the table status for each table within database provided through mysql connection provided.
 *
 * @param	object	$oMysql		PDO statement object.
 * @param	string	$db			Database name.
 * @param	string	$server		Name of server to add in output message.
 * @global	array 	$tCONFIG	Global configuration parameters. @see /config.default.php
 *
 * @return	array 	Array of table status.
 * @return	bool	False if a PDO exception occurs.
 */
function getTablesStatus($oMysql, $db, $server)
{
	global $tCONFIG;

	try
	{
		$oMysql->exec("USE ".$db);
		$tTablesStatus = $oMysql->query("SHOW TABLE STATUS", PDO::FETCH_ASSOC)->fetchAll();

		foreach($tTablesStatus AS $k=>$tTable)
		{
			$tTablesStatus[$tTable['Name']] = $tTable;
			unset($tTablesStatus[$k]);
		}
		sendOutput(locText('SHOWTABLESTATUS_OK', array($server)), 'vv');
		return $tTablesStatus;
	}
	catch(PDOException $e)
	{
		sendOutput(locText('SHOWTABLESTATUS_NOK', array($server)));
		sendOutput(locText('PDOERROR', array($e->getFile(), $e->getLine(), $e->getMessage())), 'vv');
		return false;
	}
}

/**
 * getCheckTable
 *
 * Execute a table check query for table provided, within database provided through mysql connection provided.
 *
 * @param	object	$oMysql			PDO statement object.
 * @param	string	$db				Database name.
 * @param	string	$table			Table name.
 * @global	array 	$tCONFIG		Global configuration parameters. @see /config.default.php
 * @global	integer	$iCheckError	The function is used within a loop and this variable has to be transmitted.
 *
 * @return	integer	The error state used beyond the loop. 0 if an error is detected, 1 if a warning is detected, same as received when no error nor warning.
 * @return	bool	False if a PDO exception occurs.
 */
function getCheckTable($oMysql, $db, $table)
{
	global $tCONFIG, $iCheckError;

	try
	{
		$oMysql->exec("USE ".$db);
		$oCheck = $oMysql->query("CHECK TABLE ".$table." ".$tCONFIG['checkTableMode'], PDO::FETCH_OBJ)->fetch();

		if($oCheck->Msg_type == 'error')
		{
			sendOutput(locText('CHECKTABLE_ERROR', array($table, $oCheck->Msg_type, $oCheck->Msg_text)));
			return 0;
		}
		elseif($oCheck->Msg_type == 'warning')
		{
			sendOutput(locText('CHECKTABLE_WARNING', array($table, $oCheck->Msg_type, $oCheck->Msg_text)));
			return 1;
		}
		else
		{
			sendOutput(locText('CHECKTABLE_OK', array($table)), 'vvv');
			return $iCheckError;
		}
	}
	catch(PDOException $e)
	{
		sendOutput(locText('CHECKTABLE_NOK', array($table)));
		sendOutput(locText('PDOERROR', array($e->getFile(), $e->getLine(), $e->getMessage())), 'vvv');
		return 0;
	}
}

/**
 * getChecksumCompare
 *
 * Calculates checksum for table provided, within database provided and compares the results for each of two mysql connections provided.
 *
 * @param	object	$oMysql1	PDO statement object.
 * @param	object	$oMysql2	PDO statement object.
 * @param	string	$db			Database name.
 * @param	string	$table		Table name.
 * @global	array 	$tCONFIG	Global configuration parameters. @see /config.default.php
 * @global	integer	$indent		Number of times to repeat indentation.
 *
 * @return	bool	True if checksum comparison matches.
 * @return	bool	False if checksum comparison does not match or a PDO exception occurs.
 */
function getChecksumCompare($oMysql1, $oMysql2, $db, $table)
{
	global $tCONFIG, $indent;

	try
	{
		$oMysql1->exec("USE ".$db);
		$oMysql2->exec("USE ".$db);

		$oChecksum1 = $oMysql1->query("CHECKSUM TABLE ".$table." ".$tCONFIG['checksumTableMode'], PDO::FETCH_OBJ)->fetch();
		$oChecksum2 = $oMysql2->query("CHECKSUM TABLE ".$table." ".$tCONFIG['checksumTableMode'], PDO::FETCH_OBJ)->fetch();

		if(!empty($oChecksum1->Checksum) && $oChecksum1->Checksum == $oChecksum2->Checksum)
		{
			sendOutput(locText('CHECKSUMTABLE_OK', array($table, $oChecksum1->Checksum)), 'vvv');
			return true;
		}
		else
		{
			if(empty($oChecksum1->Checksum))
				$issue = locText('CHECKSUMEMPTY');
			else
				$issue = locText('CHECKSUMDIFFER', array($oChecksum1->Checksum, $oChecksum2->Checksum));
			sendOutput(locText('CHECKSUMTABLE_NOK', array($table)).' '.$issue, 'vvv');
			return false;
		}

	}
	catch(PDOException $e)
	{
		sendOutput(locText('CHECKSUMTABLE_NOK', array($table)), 'vvv');
		sendOutput(locText('PDOERROR', array($e->getFile(), $e->getLine(), $e->getMessage())), 'vvv');
		return false;
	}
}

/**
 * setDropDatabase
 *
 * Drops database provided in mysql connection provided.
 *
 * @param	object	$oMysql		PDO statement object.
 * @param	string	$db			Database name.
 * @param	string	$server		Server name to use in output.
 *
 * @return	bool	True if drop database succeeds.
 * @return	bool	False if a PDO exception occurs.
 */
function setDropDatabase($oMysql, $db, $server)
{
	try
	{
		$oMysql->exec("DROP DATABASE IF EXISTS ".$db);
		sendOutput(locText('DROPDB_OK', array($db, $server)));
		return true;
	}
	catch(PDOException $e)
	{
		sendOutput(locText('DROPDB_NOK', array($db, $server)));
	    sendOutput(locText('PDOERROR', array($e->getFile(), $e->getLine(), $e->getMessage())));
	    return false;
	}
}

/**
 * setCreateDatabase
 *
 * Creates database provided in mysql connection provided.
 *
 * @param	object	$oMysql		PDO statement object.
 * @param	string	$db			Database name.
 * @param	string	$server		Server name to use in output.
 *
 * @return	bool	True if creation database succeeds.
 * @return	bool	False if a PDO exception occurs.
 */
function setCreateDatabase($oMysql, $db, $server)
{
	try
	{
		$oMysql->exec("CREATE DATABASE IF NOT EXISTS ".$db);
		sendOutput(locText('CREATEDB_OK', array($db, $server)));
		return true;
	}
	catch(PDOException $e)
	{
		sendOutput(locText('CREATEDB_NOK', array($db, $server)));
	    sendOutput(locText('PDOERROR', array($e->getFile(), $e->getLine(), $e->getMessage())));
	    return false;
	}
}

/**
 * setChangeMasterData
 *
 * Executes CHANGE MASTER TO on slave to set details of replications according to master.
 *
 * @param	object	$oMysql			PDO statement object.
 * @param	array 	$tMasterData	Array of master data to use in slave configuration.
 * @global	array 	$tCONFIG		Global configuration parameters. @see /config.default.php
 *
 * @return	bool	True if query succeeds.
 * @return	bool	False if a PDO exception occurs.
 */
function setChangeMasterData($oMysql, $tMasterData)
{
	global $tCONFIG;

	try
	{
		$sChangeMasterTo = "
		CHANGE MASTER TO
			MASTER_HOST = '".$tCONFIG['masterMysqlHost']."',
			MASTER_PORT = ".$tCONFIG['masterMysqlPort'].",
			MASTER_USER = '".$tCONFIG['masterMysqlUser']."',
			MASTER_PASSWORD = '".$tCONFIG['masterMysqlPwd']."',
			MASTER_LOG_FILE = '".$tMasterData['sMasterLogFile']."',
			MASTER_LOG_POS = ".$tMasterData['iMasterLogPos'].",
			MASTER_SSL = 1,
			MASTER_SSL_CA = '".$tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_CA]."',
			MASTER_SSL_CERT = '".$tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_CERT]."',
			MASTER_SSL_KEY = '".$tCONFIG['masterPdoSettings'][PDO::MYSQL_ATTR_SSL_KEY]."'
		";
		$oQchangeMasterTo = $oMysql->exec($sChangeMasterTo);

		sendOutput(locText('CHANGEMASTERTO_OK'));
		return true;
	}
	catch(PDOException $e)
	{
		sendOutput(locText('CHANGEMASTERTO_NOK'));
		sendOutput(locText('PDOERROR', array($e->getFile(), $e->getLine(), $e->getMessage())));
		return false;
	}
}

/**
 * setStartSlave
 *
 * Starts slave server.
 *
 * @param	object	$oMysql		PDO statement object.
 *
 * @return	bool	True if query succeeds.
 * @return	bool	False if a PDO exception occurs.
 */
function setStartSlave($oMysql)
{
	sendOutput(locText('SLAVESTARTING'));
	try
	{
		$oMysql->exec("START SLAVE");
		return true;
	}
	catch(PDOException $e)
	{
		sendOutput(locText('SLAVESTART_NOK'));
		sendOutput(locText('PDOERROR', array($e->getFile(), $e->getLine(), $e->getMessage())));
		return false;
	}
}

/**
 * setStartSlave
 *
 * Stops slave server.
 *
 * @param	object	$oMysql		PDO statement object.
 *
 * @return	bool	True if query succeeds.
 * @return	bool	False if a PDO exception occurs.
 */
function setStopSlave($oMysql)
{
	sendOutput(locText('SLAVESTOPING'));
	try
	{
		$oMysql->exec("SLAVE STOP");
		sendOutput(locText('SLAVESTOP_OK'));
		return true;
	}
	catch(PDOException $e)
	{
		sendOutput(locText('SLAVESTOP_NOK'));
	    sendOutput(locText('PDOERROR', array($e->getFile(), $e->getLine(), $e->getMessage())));
	    return false;
	}
}

/**
 * getMasterStatus
 *
 * Catch master status.
 *
 * @param	object	$oMysql		PDO statement object.
 *
 * @return	object	Master status fields if query succeeds.
 * @return	bool	False if a PDO exception occurs.
 */
function getMasterStatus($oMysql)
{
	try
	{
		$oQmasterStatus = $oMysql->prepare("SHOW MASTER STATUS");
		$oQmasterStatus->execute();
		$oMasterStatus = $oQmasterStatus->fetch(PDO::FETCH_OBJ);
		$oQmasterStatus->closeCursor();
		sendOutput(locText('READSTATUS_OK', array('MASTER')), 'vv');

		if(!$oMasterStatus)
			return 0;
		return $oMasterStatus;
	}
	catch(PDOException $e)
	{
		sendOutput(locText('READSTATUS_NOK', array('MASTER')));
		sendOutput(locText('PDOERROR', array($e->getFile(), $e->getLine(), $e->getMessage())));
		return false;
	}
}

/**
 * getSlaveStatus
 *
 * Catch master status.
 *
 * @param	object	$oMysql	PDO statement object.
 *
 * @return	object	Slave status fields if query succeeds.
 * @return	bool	False if a PDO exception occurs.
 */
function getSlaveStatus($oMysql)
{
	try
	{
		$oQslaveStatus = $oMysql->prepare("SHOW SLAVE STATUS");
		$oQslaveStatus->execute();
		$oSlaveStatus = $oQslaveStatus->fetch(PDO::FETCH_OBJ);
		$oQslaveStatus->closeCursor();
		sendOutput(locText('READSTATUS_OK', array('SLAVE')), 'vv');

		if(!$oSlaveStatus)
			return 0;
		return $oSlaveStatus;
	}
	catch(PDOException $e)
	{
		sendOutput(locText('READSTATUS_NOK', array('SLAVE')));
		sendOutput(locText('PDOERROR', array($e->getFile(), $e->getLine(), $e->getMessage())));
		return false;
	}
}

?>