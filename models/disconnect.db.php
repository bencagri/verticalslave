<?php

/**
 * Database disonnections
 *
 * package     VerticalSlave
 * @category    models
 * @author      ZalemCitizen
 *
 * Note:
 * The locks put on mysql tables during the execution of script are automatically removed when closing mysql session.
 * Though, the mysqldump --lock-all-tables command creates a global READ lock and removes it by itself when finished.
 *
 */

sendOutput(setAsTitle('Terminaison'), 'vvv');

if($oMysqlMaster instanceof PDO)
{
	// Déconnexion MASTER
	$oMysqlMaster = null;
	sendOutput(locText('DBDISCONNECT', array('MASTER')), 'vvv');
}

if($oMysqlSlave instanceof PDO)
{
	// Connexion SLAVE
	$oMysqlSlave = null;
	sendOutput(locText('DBDISCONNECT', array('SLAVE')), 'vvv');
}

?>