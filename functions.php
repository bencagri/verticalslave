<?php

/** 
 * General functions
 *
 * package		VerticalSlave
 * @category	common
 * @author 		ZalemCitizen
 */

/**
 * setNoBuffer
 *
 * Tries to disable PHP output buffering
 */
function setNoBuffer()
{
	 // Turn off output buffering
	ini_set('output_buffering', 'Off');
	// Turn off PHP output compression
	ini_set('zlib.output_compression', false);
	// Implicitly flush the buffer(s)
	ini_set('implicit_flush', true);
	ob_implicit_flush(true);
	// Clear, and turn off output buffering
	while(ob_get_level() > 0)
	{
	    // Get the curent level
	    $level = ob_get_level();
	    // End the buffering
	    ob_end_clean();
	    // If the current level has not changed, abort
	    if(ob_get_level() == $level) break;
	}
	// Disable apache output buffering/compression
	if(function_exists('apache_setenv'))
	{
	    apache_setenv('no-gzip', '1');
	    apache_setenv('dont-vary', '1');
	}
}

/**
 * sendOutput
 *
 * Outputs content with or without formatting
 * 
 * @param	string	$content		Text to display.
 * @param	string	$verboseRank	Optionnal: minimum level of verbosity needed to display the content. If empty, the content is displayed anyway.
 * @param	string	$br 			Optionnal: the type of new line to append to each content displayed.
 *
 * @global	integer	$indent			Number of times to repeat indentation to prepend content displayed.
 * @global	array	$tCONFIG		General configuration parameters. @see /config.default.php
 * @global	bool	$cli			True if script is executed from CLI
 */
function sendOutput($content, $verboseRank = '', $br = "\n")//'<br />')
{
	global $indent, $tCONFIG, $cli;

	if(empty($verboseRank) || empty($tCONFIG['verbose']) || !(strpos($tCONFIG['verbose'], $verboseRank) === false))
	{
		if(substr(trim($content), 0, 1) == '#')
			$content = '<font color="red"><b>'.$content.'</b></font>';
		elseif(substr(trim($content), 0, 1) == '?')
			$content = '<font color="orange"><b>'.$content.'</b></font>';
		elseif(substr(trim($content), 0, 2) == '**' && substr(trim($content), 0, 3) != '***')
			$content = '<font color="green"><b>'.$content.'</b></font>';

		if(trim(strip_tags($content)) != '' && strpos($content, "\n") === false)
			$date = date("H:i:s ");

		$output = $date.setIndent($indent).$content.$br;

		if($cli)
			$output = utf8_decode($output);

		echo $output;
	}
}

/**
 * locText
 * 
 * Localizes texts with optionnaly variable content
 *
 * @param	string	$sId		Unique id of string in localized text array $tTexts.
 * @param	array	$tVariables Optionnal: array of variable content, numerically indexed. Ex: %0 in text is replaced by $tVariables[0].
 * 
 * @global	array 	$tTexts		Localized texts array. @see /views/locale.enu.php
 *
 * @return	string	Content localized with variable content.
 */
function locText($sId, $tVariables = array())
{
	global $tTexts;

	if(!isset($tTexts[$sId]))
		return $sId.': **LocTextError**';

	//return $tTexts[$sId];
	return preg_replace_callback('#(%[0-9]{1,2})#',
       function($tMatches) use ($tVariables)
       {
            return $tVariables[trim($tMatches[1], '%')];
       },
       $tTexts[$sId]);
}

/**
 * setAsTitle
 *
 * Format a string as a title
 *
 * @param	string	$sText	Content to format as title.
 *
 * @global	integer	$indent	Number of times to repeat indentation before content.
 *
 * @return	string	Content formatted as a title, indented as needed.
 */
function setAsTitle($sText)
{
	global $indent;

	return "\n".setIndent($indent).$sText."\n".setIndent($indent).preg_replace('#.#', '=', utf8_decode($sText));
}

/**
 * setAsMajor
 *
 * Sets a string as major title
 *
 * @param	string	$sText	Content to display as a major title.
 *
 * @return	string	Content formatted as a major title, not indented.
 */
function setAsMajor($sText)
{
	return "\n**".preg_replace('#.#', '*', utf8_decode($sText))."**\n* ".$sText." *\n**".preg_replace('#.#', '*', utf8_decode($sText))."**\n";
}

/**
 * setIndent
 *
 * Outputs an indentation
 *
 * @param	integer	$rank	Number of times to repeat indentation.
 *
 * @return	string	Indentation string.
 */
function setIndent($rank = 0)
{
	for($i=0; $i<$rank; $i++)
	{
		$indent .= "\t";
	}
	return $indent;
}

/**
 * min0
 *
 * Floors a negative integer to 0 and leaves a positive integer as such.
 *
 * @param	integer	$int	The integer to convert
 * 
 * @return	integer
 */
function min0($int)
{
    if($int < 0)
        return 0;
    else
        return $int;
}

/**
 * verticalDie
 *
 * Customize the instructions before calling built-in die function.
 *
 * @param	string	$message	Optionnal: the message to display before dying.
 *
 * @global	array 	$tCONFIG	General configuration array. Used to display globally defined message when dying. @see /config.default.php
 * @global	bool	$cli		True if script is executed from CLI.
 */
function verticalDie($message = '')
{
	global $tCONFIG, $cli;

	$sDieMessage = '<font color="red"><b>'.$message.'</b></font>'."\n<b>".$tTexts['DIEMESSAGE'].'</b>';

	if($cli)
		$sDieMessage = utf8_decode($sDieMessage);
	
	die($sDieMessage);
}

?>